function circle(ctx, x, y, radius, fill = true) {
    if (radius === 1) {
        ctx.fillRect(x, y, radius, radius);
    } else {
        ctx.beginPath();
        ctx.arc(x, y, radius, 0, Math.PI * 2);
        
        if (fill) {
            ctx.fill();
        } else {
            ctx.stroke();
        }
    }
}

function vec2(ctx, vec2, radius = 1, color = 'white', fill = true) {
    circle(ctx, vec2.x, vec2.y, radius, color, fill);
}

function triangleVerteces(ctx, triangle, radius = 1) {
    circle(ctx, triangle.a.x, triangle.a.y, radius);
    circle(ctx, triangle.b.x, triangle.b.y, radius);
    circle(ctx, triangle.c.x, triangle.c.y, radius);
}

function triangle(ctx, triangle, color = 'white', fill = true) {
    ctx.save();
    ctx.strokeStyle = ctx.fillStyle = color;
    ctx.beginPath();
    ctx.moveTo(triangle.a.x, triangle.a.y);
    ctx.lineTo(triangle.b.x, triangle.b.y);
    ctx.lineTo(triangle.c.x, triangle.c.y);
    ctx.lineTo(triangle.a.x, triangle.a.y);

    if (fill) {
        ctx.fill();
    } else {
        ctx.stroke();
    }

    ctx.restore();
}