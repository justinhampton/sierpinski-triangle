// Draw the go button
const btnCanvas = document.getElementById('button-canvas');
btnCanvas.width = 15;
btnCanvas.height = 15;
const btnCtx = btnCanvas.getContext('2d');
const t1 = new Triangle(
  new Vec2(btnCanvas.width / 2, 0),
  new Vec2(0, btnCanvas.height),
  new Vec2(btnCanvas.width, btnCanvas.height)
);
const t2 = new Triangle(
  new Vec2(btnCanvas.width / 4, btnCanvas.height / 2),
  new Vec2(btnCanvas.width / 2, btnCanvas.height),
  new Vec2((btnCanvas.width / 4) * 3, btnCanvas.height / 2)
);
triangle(btnCtx, t1);
triangle(btnCtx, t2, 'black');


const canvas = document.getElementById('canvas');
canvas.width = window.innerWidth;
canvas.height = window.innerHeight;

const ctx = canvas.getContext('2d');
ctx.strokeStyle = ctx.fillStyle = 'white';

const center = canvas.width / 2;
const topOffset = 50;
const height = 450;
const width = 450;
const mainTriangle = new Triangle(
  new Vec2(center, topOffset),
  new Vec2(center - width / 2, topOffset + height),
  new Vec2(center + width / 2, topOffset + height)
)
const defaultN = 100000;
const defaultSpeed = 1;
const defaultFractalFactor = 50;
const defaultPointSize = 1;

let points = [];
let p = mainTriangle.internalRandomPoint;
let paused = false;
let n = defaultN;
let pointCount = 0;
let speed = defaultSpeed;
let fractalFactor = defaultFractalFactor / 100;
let pointSize = defaultPointSize;
let animationId = 0;

setInterval(updateCounter, 33);
init();

function init() {
  points = [p];

  triangleVerteces(ctx, mainTriangle, 2);

  // Draw a random point within the triangle
  circle(ctx, p.x, p.y, Math.max(1, pointSize/2));
  ++pointCount;

  animate();
}

function fill() {
  if (!paused) {
    togglePause();
  }

  for (i = pointCount; i < n; ++i) {
    addPoint();
  }
}

function refill() {
  const wasPaused = paused;
  if (!paused) {
    togglePause();
  }

  const prevPoints = points;
  trash();
  points = prevPoints;

  for (i = 0; i < points.length; ++i) {
    circle(ctx, points[i].x, points[i].y, Math.max(1, pointSize/2));
  }

  if (!wasPaused) {
    togglePause();
  }
}

function togglePause() {
  paused = !paused;
  const icon = paused ? '▶' : '⏸';
  const btn = document.getElementById('pause-btn');
  btn.innerText = icon;
  btn.title = paused ? 'play' : 'pause';
  animate();
}

function addPoint() {
  // Choose a random triangle vertex
  const i = randomInt(1, 3);
  const v = i === 1 ? mainTriangle.a : i === 2 ? mainTriangle.b : mainTriangle.c;

  // Calculate mid point of line segment connecting p to the triangle vertex
  var dx = Math.abs(p.x - v.x);
  var dy = Math.abs(p.y - v.y);
  if (v.x < p.x) {
    dx *= -1;
  }
  if (v.y < p.y) {
    dy *= -1;
  }
  const midPoint = new Vec2(
    p.x + dx * fractalFactor,
    p.y + dy * fractalFactor
  );
  circle(ctx, midPoint.x, midPoint.y, Math.max(1, pointSize/2));

  p = midPoint;
  ++pointCount;
  points.push(p);
}

function animate() {
  if (paused || n <= pointCount) {
    return;
  }

  clearTimeout(animationId);

  addPoint();
  animationId = setTimeout(() => {
    animate();
  }, speed);
}

function trash() {
  clearTimeout(animationId);
  ctx.clearRect(0, 0, canvas.width, canvas.height);
  pointCount = 0;
  init();
}

function setFactor() {
  const v = document.getElementById('factor-input').value / 100;
  document.getElementById('factor-value').innerText = v;
  fractalFactor = v;
}

function setN() {
  const v = document.getElementById('n-input').value;
  n = v;
  document.getElementById('n-value').innerText = v;
}

function setPointSize() {
  const v = document.getElementById('point-size-input').value;
  // B/c we use a rectangle for single pixel points, the math
  // used converts 2 to 1 so using slightly bigger than 2 is
  // needed.
  pointSize = v == 2 ? 2.01 : v;
  document.getElementById('point-size-value').innerText = v;
  refill();
}

function setSpeed() {
  const v = document.getElementById('speed-input').value;
  speed = v;
  document.getElementById('speed-value').innerText = v;
}

function updateCounter() {
  document.getElementById('counter').innerText = pointCount.toString().padStart(6, '0').replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function reset() {
  document.getElementById('n-input').value = defaultN;  
  setN();

  document.getElementById('factor-input').value = defaultFractalFactor;
  setFactor();

  document.getElementById('speed-input').value = defaultSpeed;
  setSpeed();

  document.getElementById('point-size-input').value = defaultPointSize;
  setPointSize();

  trash();
}