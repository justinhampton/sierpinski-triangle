class Vec2 {
    constructor(x, y) {
        this.x = x;
        this.y = y;
    }
}

function inTriangle(triangle, point) {
    return pointInTriangle(
        triangle.a.x,
        triangle.a.y,
        triangle.b.x,
        triangle.b.y,
        triangle.c.x,
        triangle.c.y,
        point.x,
        point.y
    );
}

function pointInTriangle(x1, y1, x2, y2, x3, y3, x, y) {
    let denominator = ((y2 - y3) * (x1 - x3) + (x3 - x2) * (y1 - y3));
    let a = ((y2 - y3) * (x - x3) + (x3 - x2) * (y - y3)) / denominator;
    let b = ((y3 - y1) * (x - x3) + (x1 - x3) * (y - y3)) / denominator;
    let c = 1 - a - b;

    return 0 <= a && a <= 1 && 0 <= b && b <= 1 && 0 <= c && c <= 1;
}

// Function to generate a random integer
function randomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }