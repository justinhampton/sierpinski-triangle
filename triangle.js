class Triangle {
    constructor(a, b, c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    containsPoint(p) {
        return inTriangle(this, p);
    }

    get internalRandomPoint() {
        const left = this.leftVertex.x;
        const right = this.rightVertex.x;
        const top = this.topVertex.y;
        const bottom = this.bottomVertex.y;

        let p = new Vec2(
            randomInt(left, right),
            randomInt(top, bottom)
        );
        
        while (!this.containsPoint(p)) {
            p = new Vec2(
                randomInt(left, right),
                randomInt(top, bottom)
            );
        }

        return p;
    }

    get leftVertex() {
        if (this.a.x < this.b.x) {
            if (this.a.x < this.c.x) {
                return this.a;
            } else {
                return this.c;
            }
        } else if (this.b.x < this.c.x) {
            return this.b;
        } else {
            return this.c;
        }
    }

    get rightVertex() {
        if (this.a.x > this.b.x) {
            if (this.a.x > this.c.x) {
                return this.a;
            } else {
                return this.c;
            }
        } else if (this.b.x > this.c.x) {
            return this.b;
        } else {
            return this.c;
        }
    }

    get topVertex() {
        if (this.a.y < this.b.y) {
            if (this.a.y < this.c.y) {
                return this.a;
            } else {
                return this.c;
            }
        } else if (this.b.y < this.c.y) {
            return this.b;
        } else {
            return this.c;
        }
    }

    get bottomVertex() {
        if (this.a.y > this.b.y) {
            if (this.a.y > this.c.y) {
                return this.a;
            } else {
                return this.c;
            }
        } else if (this.b.y > this.c.y) {
            return this.b;
        } else {
            return this.c;
        }
    }
}